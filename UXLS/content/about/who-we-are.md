+++
date = "2017-06-01T13:01:36+02:00"
title = "Who We Are"
weight = 100

[menu.main]
parent = "about"
+++

UXLS is a community of practitioners from leading life science, pharmaceutical and software companies. We have over 80 members contributing to UXLS. For more information, publications and upcoming events around UX in the Life Sciences, please [visit our Pistoia Alliance project site](https://www.pistoiaalliance.org/projects/current-projects/user-experience-for-life-sciences/).

## Created by leading life sciences companies

{{< organisations >}}

## Project Champions

We are grateful for the ongoing support of our project champions who continue to support the UX cause in the life sciences.

{{< team-members >}}

## Project Champion Alumni

The UXLS Project has benefitted from some fantastic UX champions and supporters. We continue to be grateful for their contribution.

{{< uxls-supporters >}}