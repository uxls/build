+++
date = "2015-01-26T15:41:01+02:00"

+++


# Online Privacy Statement

Last Updated: January 26, 2015

Your privacy is important to Pistoia Alliance, Inc. (“Pistoia”). Our goal is to provide you with a personalized online experience that provides you with the information, resources, and services that are most relevant and helpful to you. This Privacy Statement has been written to describe the conditions under which this website is being made available to you. The Privacy Statement discusses, among other things, how data obtained during your visit to this website may be collected and used. We strongly recommend that you read the Privacy Statement carefully. By using this website, you agree to be bound by the terms of this Privacy Statement. If you do not accept the terms of the Privacy Statement, you are directed to discontinue accessing or otherwise using the website or any materials obtained from it. If you are dissatisfied with the website, by all means contact us at comms@pistoiaalliance.org; otherwise, your only recourse is to disconnect from this site and to refrain from visiting the site in the future.

## GENERAL TERMS

### Sites Covered by this Privacy Statement

This Privacy Statement applies to all websites, domains, information portals, and registries maintained by Pistoia.

### Changes to this Privacy Policy

Pistoia may decide at some point in the future, without advance notice, to modify this Privacy Policy by posting a new Policy at this site and noting the date upon which the new policy is effective and/or by sending you an email with the new terms. Please review the changes to our Policy carefully. If you agree to the terms, simply continue to use the Services. If you object to any of the changes to the Privacy Policy, please do not continue to use the Services, as your continued use of the Services after we’ve posted a notice of changes to the Privacy Policy shall constitute your consent to the changed terms or practices.

### Children’s Privacy

Pistoia is committed to protecting the privacy needs of children, and we encourage parents and guardians to take an active role in their children’s online activities and interests. Pistoia does not intentionally collect information from children under the age of 13, and Pistoia does not target its Services to children. Only persons who are more than 18 years old or who are emancipated minors may use the Services. By accessing the services, you are legally acknowledging that you are over the age of 18 or an emancipated minor. Otherwise, you have no legal right to access or use the site or the services.

### California Privacy Laws

California residents who provide Personal Information in obtaining products or services for personal, family or household use are entitled to request and obtain from us, once per calendar year, information about the customer information we shared, if any, with other businesses for their own direct marketing uses. If applicable, this information would include the categories of customer information and the names and addresses of those businesses with which we shared customer information for the immediately prior calendar year. To obtain this information from us, please send us an email with “Request for California Privacy Information” as the subject of your message, and we will send you a reply e-mail containing the requested information. Not all information sharing is covered by the “Shine the Light” requirements and only information on covered sharing will be included in our response.

Please note that Pistoia does not respond to “do not track” signals or other similar mechanisms intended to allow California residents to opt-out of Internet tracking. Pistoia also may track and/or disclose your online activities over time and across different websites when you use our Services.

### Security

We have implemented industry-standard security safeguards designed to protect the Personal Information that you may provide. We also periodically monitor our system for possible vulnerabilities and attacks, consistent with industry standards. You should be aware, however, that since the Internet is not a 100% secure environment, we cannot ensure or warrant the security of any information that you submit to us. There’s also no guarantee that information may not be accessed, disclosed, altered, or destroyed by breach or failure of any of our physical, technical, or managerial safeguards. It’s your responsibility to protect the security and integrity of your account details, including your username and password, and limiting access to your device and browser by signing off after you have finished accessing your account. Please note that emails, instant messaging, and similar means of communication with other users of our Services are not encrypted, so you should not communicate any confidential information through these means.

### Links to Third Party Websites

Pistoia’s website may provide links to third-party websites for the convenience of our users. If you access those links, you will leave Pistoia’s website. Pistoia does not control these third-party websites and cannot represent that their policies and practices will be consistent with this Privacy Statement. For example, other websites may collect or use personal information about you in a manner different from that described in this document. Therefore, you should use other websites with caution, and you do so at your own risk. We encourage you to review the privacy policy of any website before submitting personal information.

## TYPES OF INFORMATION WE COLLECT

### Non-Personal Information

Non-personal information is data about usage and service operation that is not directly associated with a specific personal identity. Pistoia may use non-personal data that is aggregated for reporting about website usability, performance, and effectiveness of our website. It may be used to improve the experience, usability, and content of the site and our Services. Pistoia may automatically receive and record information on our server logs from your browser or mobile platform, including your location, IP address, browser type, operating information, mobile carrier, device and application IDs, cookie information, and an API you may use. By continuing to use our site or services, you consent to our use of cookies, pixels and local storage and other data collection technologies in accordance with this Privacy Policy.

### Aggregate Information

Pistoia may gather aggregate information, which refers to information your computer automatically provides to us and that cannot be tied back to you as a specific individual. Examples include referral data (the websites you visited just before and just after our site), the pages viewed, time spent at our website, and Internet Protocol (IP) addresses. An IP address is a number that is automatically assigned to your computer whenever you access the Internet. For example, when you request a page from one of our sites, our servers log your IP address to create aggregate reports on user demographics and traffic patterns and for purposes of system administration.

### Log Files

Every time you request or download a file from the website, Pistoia may store data about these events and your IP address in a log file. We may use this information to analyze trends, administer the website, track users’ movements, and gather broad demographic information for aggregate use or for other business purposes.

### Cookies

Our site may use a feature of your browser to set a “cookie” on your computer. Cookies are small packets of information that a website’s computer stores on your computer. Pistoia’s website can then read the cookies whenever you visit our site. We may use cookies in a number of ways, such as to save your password so you don’t have to re-enter it each time you visit our site, to deliver content specific to your interests and to track the pages you’ve visited. These cookies allow us to use the information we collect to customize your experience so that your visit to our site is as relevant and as valuable to you as possible.

Most browser software can be set up to deal with cookies. You may modify your browser preference to provide you with choices relating to cookies. You have the choice to accept all cookies, to be notified when a cookie is set or to reject all cookies. If you choose to reject cookies, certain of the functions and conveniences of our website may not work properly, and you may be unable to use those of our services that require registration in order to participate, or you will have to re-register each time you visit our site. Most browsers offer instructions on how to reset the browser to reject cookies in the “Help” section of the toolbar. We do not link non-personal information from cookies to personally identifiable information without your permission.

### Web Beacons

Pistoia’s website also may use web beacons to collect non-personal information about your use of our website and the websites of selected sponsors or members, your use of special promotions or newsletters, and other activities. The information collected by web beacons allows us to statistically monitor how many people are using our website and selected sponsors’ sites; how many people open our emails; and for what purposes these actions are being taken. Our web beacons are not used to track your activity outside of our website or those of our sponsors. Our website does not link non-personal information from web beacons to personally identifiable information without your permission.

### Clickstream

The site may allow for the use of clickstream data collection. Clickstream analysis is the process of collecting, analyzing, and reporting aggregate data about which pages visitors visit in what order based on mouse clicks each user makes. Clickstream analysis is useful for web activity analysis and other functions that help us to understand how you use our site. We do not sell users’ clickstream data to third parties for marketing purposes.

### New Technologies

As new technologies emerge, Pistoia may be able to improve our services or provide you with new ones, which means that Pistoia may create new ways to collect information on the site. If we offer a new service or new features to our existing site, for example, these changes may result in our collecting new information in order to improve your user experience.

### Personal Information

Personal information is information that is associated with your name or personal identity. Pistoia uses personal information to better understand your needs and interests and to provide you with better service. On some of our web pages, you may be able to request information, subscribe to mailing lists, participate in online discussions, collaborate on documents, provide feedback, submit information into registries, register for events, apply for membership, or join technical committees or working groups. The types of personal information you provide to us on these pages may include name, address, phone number, e-mail address, user IDs, passwords, or billing information. Pistoia may use personal information to provide services that support the activities of our members and their collaboration on our projects. When accessing our members-only web pages, your personal user information may be tracked by Pistoia in order to support collaboration, ensure authorized access, and enable communication between members.

### Registration

In order to use some features of the Pistoia website, you need to create an account by providing use with at least your name, email address, user name, and a password. You can choose to provide other information about yourself during the registration process (for example, your gender, location, company affiliation, etc.). We use this additional information to provide you with more customized services, and this information may be viewable by others. You understand that, by creating an account, Pistoia and others will be able to identify you by your profile, and you agree to allow Pistoia to use this information in accordance with this Privacy Policy and our Terms of Use.

On some pages of the site, you may be able to request information, subscribe to mailing lists, participate in online discussions, collaborate on documents, provide feedback, submit information into registries, register for events, apply for membership, or join technical committees or working groups. The types of personal information you provide to us on these pages may include name, address, phone number, e-mail address, user IDs, passwords, billing information, or credit card information.

Credit card information may be collected to facilitate membership applications, but we do not retain this information on our servers. Credit card numbers are used only for processing payment and are not used for other purposes. Payment processing services are provided by a third-party payment service, and a management company external to the Company may provide support for these financial activities. We may share your personal information with these partners to facilitate these transactions.

### Account Profile Information

Once you’ve created an account, you may choose to provide additional information on your user profile, such as descriptions of your job title, professional experience, your educational background, professional affiliations and memberships, and technical skills. This information that you voluntarily provide may be seen by other users.

### Members-Only Website

Pistoia may provide a members-only section of our website. Information you provide on Pistoia’s membership application may be used to create a member profile, and some information may be shared with other of our individual member representatives and organizations. Member contact information may be provided to other members on a secure website to encourage and facilitate collaboration, research, and the free exchange of information among our members, but we expressly prohibit members from using member contact information to send unsolicited commercial correspondence. Pistoia’s members automatically are added to our member mailing lists. From time to time, member information may be shared with event organizers and/or other organizations that provide additional benefits to our members. By providing us with your personal information on the membership application, you expressly consent to our storing, processing, and distributing your information for these purposes.

### Company Information

Company information is information that is associated with the name and address of member organizations and may include data about usage and service operation. The primary representative of any our member organizations may request usage reports to gauge the extent of their employees’ involvement in consortium activities. You should be aware that information regarding your participation in technical committees or working groups, for example, may be made available to your company’s primary representative and to Pistoia’s staff members.

## HOW WE USE YOUR INFORMATION

When you join Pistoia, you acknowledge that information you provide on your membership profile may be seen by others and used by Pistoia as described in this Privacy Policy and our Terms of Use.

### Consent to Use by Pistoia

Pistoia may use personal information to provide services that support the activities of the organization, Pistoia members, and their collaboration on Pistoia activities. When accessing the site, your personal user information may be tracked by Pistoia in order to support collaboration, ensure authorized access, and enable communication between members.

The personal information you may provide to Pistoia may reveal or allow others to discern aspects of your life that are not expressly stated in your profile (for example, your picture or your name may reveal your gender). By providing personal information to us when you create or update your account and profile, you are expressly and voluntarily accepting the terms and conditions of our Terms of Use and freely accepting and agreeing to our processing of your personal information in ways set out by this Privacy Policy. Supplying information to us, including any information deemed “sensitive” by applicable law, is entirely voluntary on your part. You may withdraw your consent to Pistoia’s collection and processing of your information by changing closing your account.

### Communications from Pistoia

We use the information you provide to customize your experience on the site. We may communicate with you using email or other means available to us regarding the availability of services, service-related issues, or promotional messages that we believe may be of interest to you. We may, for example, send you welcome messages, emails regarding new features or services, and promotional information from Pistoia, or our affiliates, members, and partners. You may opt out of receiving promotional messages from Pistoia by following the instructions contained in the email. As long as you’re a registered user, however, you can’t opt out of receiving service messages from us. Pistoia may also use personal information in order to customize content on the site to you, such as news relevant to you or to your industry or company.

### Communications from Others

Member contact information may be provided to other members on a secure site to encourage and facilitate collaboration, research, and the free exchange of information among our members. Please remember that any information (including personal information) that you disclose on our site, such as forums, message boards, and news groups, becomes public information that others may collect, circulate, and use. Because we cannot and do not control the acts of others, you should exercise caution when deciding to disclose information about yourself or others in public forums such as these.

### Sharing Information with Members and Affiliates

Pistoia may share your personal information with our members and affiliates, as necessary to provide you with the services on the site. From time to time, member information may be shared with event organizers and/or other organizations that provide additional benefits to our members. By providing us with your personal information during the user registration process and by agreeing to the terms of this Privacy Policy, you expressly consent to our storing, processing, and distributing your information for these purposes.

### Sharing Information with Third Parties

Information you put on your profile and any content you post on the site may be seen by others. In keeping with our open process, Pistoia may maintain publicly accessible archives for our activities. For example, posting an email to any of Pistoia’s hosted mail lists or discussion forums, subscribing to one of our newsletters or registering for one of our public meetings, may result in your email address becoming part of the publicly accessible archives.

Content contained on the site may result in display of some of your personal information outside of Pistoia. For example, when you post content to a forum that is open for public discussion, your content, including your name as the contributor and your email address, may be displayed in search engine results. In addition, your public profile may be indexed and displayed through public search engines when someone searches for your name.

You are responsible for any information you post on the site, and this content may be accessible to others. Accordingly, you should be aware that any information you choose to disclose on the site can be read, collected, and used by other users in the forum, and in the case of forums open to the public, by third parties. Pistoia is not responsible for the information you choose to submit on the site.

Pistoia does not rent or sell or otherwise distribute personal information that you have shared with us, except as permitted in this Privacy Policy and our Terms of Use. We will not disclose personal information that is associated with your profile unless Pistoia has a good faith belief that disclosure is permitted by law or is reasonably necessary to: (1) comply with a legal requirement or process, including, but not limited to, civil and criminal subpoenas, court orders or other compulsory disclosures; (2) investigate and enforce this Privacy Policy or our Terms Use; (3) respond to claims of a violation of the rights of third parties; (4) respond to member service inquiries; (5) protect the rights, property, or safety of Pistoia, our users, or the public; or (6) as part of the sale of the assets of Pistoia or as a change in control of the organization or one of its affiliates or in preparation for any of these events. Pistoia reserves the right to supply any such information to any organization into which Pistoia may merge in the future or to which it may make any transfer in order to enable a third party to continue part or all of the organization’s mission. Any third party to which Pistoia transfers or sells its assets will have the right to use the personal and other information that you provide in the manner set out in this Privacy Policy.

### Polls and Surveys

Pistoia may conduct polls and surveys of our users, and your participation in this type of research is at your sole discretion. Pistoia may follow up with you regarding your participation in this research. You may at any time opt out of participating in our polls and surveys.

### Information Sharing

Pistoia does not sell, rent, or lease any individual’s personal information or lists of email addresses to anyone for marketing purposes, and we take commercially reasonable steps to maintain the security of this information. However, Pistoia reserves the right to supply any such information to any organization into which Pistoia may merge in the future or to which it may make any transfer in order to enable a third party to continue part or all of its mission. We also reserve the right to release personal information to protect our systems or business, when we reasonably believe you to be in violation of our Terms of Use or if we reasonably believe you to have initiated or participated in any illegal activity. In addition, please be aware that in certain circumstances, Pistoia may be obligated to release your personal information pursuant to judicial or other government subpoenas, warrants, or other orders.

In keeping with our open process, Pistoia may maintain publicly accessible archives for the vast majority of our activities. For example, posting an email to any of Pistoia’s hosted mail lists or discussion forums, subscribing to one of our newsletters or registering for one of our public meetings, may result in your email address becoming part of the publicly accessible archives. On some websites, anonymous users are allowed to post content and/or participate in forum discussions. In such a case, since no user name can be associated with such a user, the IP address number of a user is used as an identifier. When posting content or messages to the sites anonymously your IP address will be revealed to the public.

If you are a registered member of Pistoia, you should be aware that some items of your personal information may be visible to other members and to the public. Pistoia’s member database may retain information about your name, e-mail address, company affiliation (if an organizational member), and such other personal address and identifying data as you choose to supply. That data may be generally visible to other of our members and to the public. Your name, e-mail address, and other information you may supply also may be associated in Pistoia’s publicly accessible records with our various committees, working groups, and similar activities that you join, in various places, including: (i) the permanently-posted attendance and membership records of those activities; (ii) documents generated by the activity, which may be permanently archived; and, (iii) along with message content, in the permanent archives of Pistoia’s e-mail lists, which also may be public.

Please remember that any information (including personal information) that you disclose in public areas of our website, such as forums, message boards, and news groups, becomes public information that others may collect, circulate, and use. Because we cannot and do not control the acts of others, you should exercise caution when deciding to disclose information about yourself or others in public forums such as these.

## DATA PROTECTION

Given the international scope of Pistoia, personal information may be visible to persons outside your country of residence, including to persons in countries that your own country’s privacy laws and regulations deem deficient in ensuring an adequate level of protection for such information. If you are unsure whether this privacy statement is in conflict with applicable local rules, you should not submit your information. If you are located within the European Union, you should note that your information will be transferred to the United States, which is deemed by the European Union to have inadequate data protection. Nevertheless, in accordance with local laws implementing the European Union Privacy Directive on the protection of individuals with regard to the processing of personal data and on the free movement of such data, individuals located in countries outside of the United States of America who submit personal information do thereby consent to the general use of such information as provided in this Privacy Policy and to its transfer to and/or storage in the United States of America. By utilizing the site and/or directly providing personal information to us, you hereby agree to and acknowledge your understanding of the terms of this Privacy Policy, and consent to have your personal data transferred to and processed in the United States and/or in other jurisdictions as determined by Pistoia, notwithstanding your country of origin, or country, state and/or province of residence. If you do not want your personal information collected and used by Pistoia, please do not visit Pistoia’s website or apply for membership.

## YOUR OPTIONS AND OBLIGATIONS

Rights to access, correct, or delete your information; closing your account.

You may access, modify, correct, or delete your personal information controlled by Pistoia regarding your profile or close your account. You can also contact us for any account information which is not on your profile or readily accessible to you. If you close your account, all of your content will remain visible on the site.

You should be aware that information that you’ve shared with others or that others have copied may also remain visible after you have closed your account or deleted the information from your own profile. In addition, you may not be able to access, correct, or eliminate any information about you that other users have copied or exported out of the site, because this information may not be in our organization’s control.

### Data Retention

We will keep your information for as long as your account is active or as needed to comply with our legal obligations, even after you’ve closed your account, such as to meet regulatory requirements, resolve disputes between users, to prevent fraud and abuse, or to enforce this Privacy Policy and our Terms of Use. We may be required to retain personal information for a limited period of time if requested by law enforcement. We also may retain indefinitely non-personally identifiable, aggregate data to facilitate our ongoing operations.

### Your Obligations

Be respectful and courteous. Pistoia is a community group, and you have certain obligations both to Pistoia and to your fellow users.

In order to ensure the integrity of the Pistoia community effort, you must respect the terms of our Privacy Policy, our Terms of Use, any other applicable policies of the Pistoia, as well as the rights of other community users, including their intellectual property rights.

You must not upload or otherwise disseminate any information that may infringe on the rights of others or that may be deemed to be defamatory, injurious, violent, offensive, racist or xenophobic, or that may otherwise violate the purpose and community spirit of Pistoia or its members.

If you violate any of these guidelines or those detailed in our Terms of Use, the Pistoia may, at its sole discretion, suspend, restrict, or terminate, your account and your ability to access the site.

### Opting Out

From time to time Pistoia may email you electronic newsletters, announcements, surveys or other information. If you prefer not to receive any or all of these communications, you may opt out by following the directions provided within the electronic newsletters and announcements.

### Contacting Us

Questions about this Privacy Statement can be directed to privacy@pistoiaalliance.org