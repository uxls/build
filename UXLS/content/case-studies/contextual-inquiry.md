+++
date = "2017-06-01T14:06:12+02:00"
author = "Novartis"
company = "Novartis"
featured = true
title = "Seeing Scientists Work"
methods = ["contextual-inquiry.md"]
summary = "See how Novartis helped scientists be more productive in the lab and spend less time on administrative tasks by observing their work directly and asking the right questions."
image = "contextual_inquiry.jpg"
companylogo = ""

[menu.main]
  parent = "case-studies"

[menucontent]
  image = "contextual_inquiry.jpg"

[[sidebar]]
    title = "Team"
    content = ["2 UX researchers", "2 bench scientists"]

[[sidebar]]
    title = "Timeline"
    content = ["3-4 weeks"]

[[sidebar]]
    title = "Deliverables"
    content = ["Analysis Report (.ppt)"]
+++

{{< section sidebar="true" style="content" >}}

## Seeing Scientists Work

At Novartis, scientists work on multiple facets of the same project in different locations, like performing different tasks at their desks and at their labs. Therefore, not only are the locations different, but also the tools, interactions, and behaviors.

It is important to observe users in their work environment to get the first-hand experience of their daily tasks and workflows. Often seeing someone at work provides more detailed and higher quality insights than just hearing about it.

The goals for this research were to discover the underlying issues with high error rate while capturing scientific data, to close workflow gaps between scientists and locations, and to increase scientists’ efficiency overall.
{{< /section >}}

{{< section small="12" medium="6" >}}
{{< col >}}

## Process

### Plan

UX researchers planned to shadow scientists starting at their labs as they were working on their experiments or measuring data, then at their desks where they either prepared or analyzed their studies. The researchers reached out to scientists and explained the purpose and goals and asked for participation. By participating, scientists agreed to let the team shadow them, with researchers asking questions along the way for clarification.

### Conduct

Researchers started by asking some introductory questions about their role and context, followed by the observation session equipped with paper, pencil, and a camera to document key observations. 2-3 sessions were held over 2 weeks, based on their availability and tasks planned for the day. An iterative dialog between the researchers and scientists about specific observations and questions helped to confirm understanding. This also provided an opportunity to share any feedback or ideas they might have about the new workflow.

### Analyze and Report

Researchers visualized the detailed workflows by drawing them into a cartoon. This helped to show how complicated workflows can be in the labs. They also gathered all the notes on post-its and grouped them into categories such as major pain points, time consuming tasks, causes for high error rates, as well as things that were working smoothly. In this way, they identified areas that can be improved in their scientists’ daily work.

One of the major discoveries was how time and energy consuming it is to write test results and labels on the tubes manually in the lab while wearing protective clothing and gloves. Also, scientists found it very difficult to manage data scattered between paper, desktop computers, and labs, and redundant data entry. These difficulties increased error rate in data, made scientists inefficient, and contributed to a generally unsatisfying experience.
{{< /col >}}
{{% img src="contextual_inquiry_observation.jpg" caption="Observations" %}}
{{% img src="contextual_inquiry_workflow.jpg" caption="Concept workflow" %}}
{{< /section >}}

{{< section style="content" >}}

## Outcome

Based on this research, the team developed design concepts for an improved workflow. This improved workflow included new hardware and software for desktop computers and mobile devices. They reviewed and iterated with the scientists until a solution was developed that worked for them.

The new solution includes a mobile device with software that allows Novartis scientists to scan the barcoded data in the lab while being constantly in sync with the desktop computer system, and can trigger remote printing for tube labels. Because scientists were wearing gloves in the lab, the touchscreen interaction was designed to avoid gestures that did not work well with the gloves, and supplied extra touch screen stylus pens. Users found that the solution was easy to use, less error-prone, and not limiting while wearing protective gear. This increased efficiency allowed scientists for more time doing scientific research, and less on administrative tasks.
{{< /section >}}
