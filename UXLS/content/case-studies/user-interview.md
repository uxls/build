+++
date = "2017-06-01T14:06:12+02:00"
author = "Amgen"
company = "Amgen"
featured = true
title = "Understanding Bench Scientists"
methods = ["user-interviews.md"]
summary = "Find out how Amgen used interviews to gain insights around how their bench scientists collaborate and find ways to reduce barriers to innovation."
image = "amgen_header.jpg"
companylogo = "pa-members-logos/amgen-logo.png"

[menu.main]
parent = "case-studies"

[menucontent]
image = "amgen_header.jpg"

[[sidebar]]
    title = "Team"
    content = ["2 UX researchers", "1 business stakeholder", "10 bench scientists"]

[[sidebar]]
    title = "Timeline"
    content = ["6 weeks total"]

[[sidebar]]
    title = "Deliverables"
    content = ["Detailed Findings and Recommendations", "Bench Scientist Persona"]
+++

{{< section sidebar="true" style="content" >}}
## Understanding Bench Scientists

Amgen believes that breakthrough discoveries are driven by productive bench scientists. The User Experience (UX) research team wanted to better understand how our scientists collaborate with each other, how they transition between their offices and labs to do work and identify opportunities to foster productivity.

Insights gathered through speaking directly with scientists in their work environments could inform system owners, support teams and designers as they develop solutions.
{{< /section >}}

{{< section >}}
## Process

### Plan

Over the course of two weeks, the UX research team worked with the business stakeholder to understand and prioritize interview goals and topics to explore. They also developed a discussion guide to help conduct the interviews based on those goals. Finally, they identified and scheduled interviewees with a mix of expertise (e.g. large and small molecule), experience level and geographic location.

### Conduct

A total of ten interviews were conducted over the next two weeks. These interviews were face-to-face in the office/lab. With permission, the interviewers recorded the sessions using a portable voice recorder and took additional notes. In some cases they also toured their labs and took a few photos, which really helped to see what the specific environment is like.

### Analyze and Report

After each interview, the UX researchers reviewed the recordings and compiled more detailed notes. Once all the interviews were complete, they identified common themes, unexpected insights and followed up for clarification as needed. The final report was delivered to the business stakeholder in about a week and included findings and recommendations, along with key quotes (anonymized). Ultimately, the findings were incorporated into a Bench Scientist persona, which, along with other Amgen role-based personas, are used by internal teams to better consider key roles, like scientists, as they develop solutions.
{{< /section >}}

{{< section >}}
{{% img src="interview.jpg" %}}
{{< /section >}}

{{< section style="content" >}}
## Outcome

Through the interviews the team learned that most scientists believed that collaboration fosters innovation and that the lab is where they have the best opportunity to connect with other researchers. They also learned that scientists need a quiet area to focus with sufficient space to view information like data and papers (both on-screen and off). Additionally, the UX research team recommended that any designs consider the fact that scientists wear Personal Protective Equipment (PPE) ( i.e. gloves, goggles etc.) in the lab, so that solutions minimize the need to take off/put on PPE while performing tasks.

There were a few insights that were really useful. For example, the team learned that because the process to order supplies was difficult and time-consuming, scientists tended to over-order items and store them in the lab, leading to cramped conditions. Making that process easier enabled scientists to spend less time worrying about ordering and storing supplies and reduced clutter in the lab.

The findings and persona were ultimately used to make decisions around the redesign of the lab and office spaces to better enable collaboration and maximize the efficiency of the physical space.
{{< /section >}}
