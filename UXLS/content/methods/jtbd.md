+++
date = "2018-01-08T06:22:09+01:00"
featured = false
title = "Jobs To Be Done"
short_title = "JTBD"
weight = "300"

[header]
  thumbnail = "jtbd.jpg"
  image = "jtbd.jpg"
  summary = "Find out what jobs users want to get done so that you can design tools, products and services that provide better solutions."

[menucontent]
  section = "user-research"

  [menu.main]
    parent = "methods"

[sidebar]
  difficulty = "Medium"
  project_stage = "User Research"
  great_for = ["Evaluating, designing, and identifying ways to improve new tools, products and services", "Identifying and prioritising desirable features"]
  materials = ["Large rolls of paper", "Post-it notes", "Jobs to be done canvas"]
  resources = [""]
  takes = "2-4 days"

[capabilities]
  user_reqs = "yes"
  focus = "no"
  evaluate = "no"
  generate = "no"
  measure_ux = "no"
  compare = "no"

+++

## Overview

Jobs to be done is a framework for identifying and exploring the key tasks and jobs that users need to get done. It focuses on what users want to get done, rather than just looking at how they go about doing it.

Jobs to be done can help to identify what is important to users and to design tools, products and services that provide better solutions. The framework is best explored through interviews to discover jobs to be done with users, a workshop to analyse and map out jobs, and then a survey or follow up workshop to help prioritise jobs and potential solution criteria. These are succinct ways to describe jobs to be done, and can be a useful addition or even replacement for Agile user stories

### Who’s involved

- Facilitator
- Note taker
- Optional subject matter experts
- Project team members

## How To

### I. Plan and Prepare

1. **Identify and prioritize goals**\
Determine what is it you want to understand and focus on an area of interest.
2. **Choose an area of focus**\
Determine the area you want to examine using the jobs to be done framework. This might be a specific task, or a broader area - such as the work of a lab scientist within your organisation.
3. **Identify user groups and select participants**\
Determine the users to interview. Aim for a representative cross-section of 6-10 users. Ensure that users have direct experience of the area of focus.
4. **Schedule interviews**\
Interviews are best carried out face to face in the user’s environment. If face to face interviews are not possible aim for video calls. Interviews are likely to take 30 – 60 mins, although this can vary depending on the area of focus.
5. **Create interview guide**\
This should be a list of possible questions to ask and topics to cover, rather than a set script. It’s also useful to outline how to introduce and close interviews.
6. **Schedule jobs to be done workshop**\
The workshop is best carried out face to face, although remote workshops are possible. Invite those that were involved in the jobs to be done interviews, along with some subject matter experts and key team members (e.g. product owner). Try to keep the group to no more than 6-7 people.
7. **Prepare workshop material**\
Large rolls of paper and post-it notes are useful for capturing details. Different colours of post-it notes can be used for different sorts of information.

#### Tips:

- If getting access to users is extremely difficult, as a last resort, consider speaking to subject matter experts or ex-users as proxy users.
- Try to schedule at least 2 interviews with each distinct user group. For example, lab scientists and principal investigators.
- Consider scheduling a few backup interviews in case some of your participants are unavailable.

### II. Carry out jobs to be done interviews

1. **Welcome participants and provide brief of session**\
Start with a summary about their background and their current role.
2. **Explore the area of focus based on interview guide**\
The questions you ask will differ depending on how broad or narrow your area of focus is. If you’re looking at opportunities to innovate by identifying jobs to be done, you might explore jobs they struggle with, work arounds they have, or existing tools, products and services that are not being used.
3. **Close the interview**\
Thank the participants for their time and remind them of the purpose of the interviews.

#### Tips:

- Treat each interview as a conversation, rather than a set script. Keep to the area of focus, but look to explore interesting points and topics with participants.
- Record interviews---be sure to ask permission from participants and be aware of company policies regarding recording.
- Give yourself at least 30 minutes between interviews to go through notes and to allow for any run over.
- Consider having someone facilitate the interview and another to capture information.
- Review the interview notes and recordings as soon as possible. Start to pull out key themes and insights that can feed into the jobs to be done workshop.

### III. Run the jobs to be done workshop

1. **Welcome participants and provide brief of session**\
Introduce the concept of jobs to be done, and outline why you’re interested in exploring the particular area of focus.
2. **Use jobs stories to frame the main job to be done**\
Jobs stories take the form of a situation (When…), need (I want to…) and goal (So…). A simple jobs stories template is available.
3. **Step through each job to be done and capture related jobs**\
For each step, identify and note any related jobs. Focus on what users need to get done, rather than how they achieve it.
4. **Capture criteria for assessing possible solutions**\
Criteria can be functional, emotional or social. Functional criteria relate to practical criteria that users apply. Emotional criteria relate to how users want to feel. Social criteria is how users want to be perceived by others.
5. **Capture pain points, possible solutions, and opportunities**\
Identify opportunities to better support users and to improve existing tools, products and services. Walk through each step and ask the group how can the job can be better supported.
6. **Close the workshop**\
Wrap up the workshop. Thank all the participants and outline any next steps.

#### Tips:

- For long workshops schedule in breaks every 1 to 1.5 hours.
- Focus on the main job to be done, but capture other key jobs. If there isn’t time in the workshop to explore these, think about setting up a follow workshop.
- Take note or photograph post-it notes in-case a few go stray.

### IV. Analyze and Report

1. **Document and share jobs to be done**\
One way to do so is: jobs to be done canvas. Share the workshop outputs with the participants and ask if there are any additional remarks to add.
2. **Review questions and assumptions**\
Often questions and assumptions will come up during a workshop that will require further investigation. Go through these and consider the best approach for answering them.
3. **Prioritise jobs to be done and criteria**\
In the form of a workshop or survey, ask users to rate the importance of jobs and criteria identified. Consider their level of satisfaction with current solutions. Jobs can then be mapped against importance and satisfaction using a simple jobs prioritisation matrix.
4. **Evaluate against jobs to be done and identify unmet needs**\
Having explored what users want to get done, you can evaluate existing tools, products and services to determine how well they fit the job and solution criteria.

#### Tips:

- Document and share jobs to be done as soon as possible. This will ensure that it is still fresh in everyone’s minds.
- Job stories are a great way to capture user requirements. You can use them to provide valuable context for features.


## Tips for Life Sciences

- Ensure that you speak to users in their respective working environment. This will also allow them to physically show you some of the current solutions they use.
- Embrace the complexity that often comes with Life Sciences based jobs to be done. Capture as much detail as possible in workshops and use time afterwards to reflect back.
- Consider working with subject matter experts to help analyse and interpret complex jobs to be done as this will help provide background on terminology and scientific procedures.
- When encountering complex domains like Life Sciences, be sure to ask questions to users to investigate further if need be.


## Resources

- [Job stories template (doc)](Job stories template.docx) – A simple template for writing job stories
- [Jobs to be done Canvas (doc)](Jobs to be done canvas.docx) – A one page canvas to capture key details for jobs to be done
- [Jobs to be done Canvas example (doc)](Jobs to be done canvas - example.docx) – Example jobs to be done canvas
- [Jobs to be done prioritisation matrix (doc)](Jobs prioritisation matrix.docx) – Matrix for prioritising jobs to be done by importance and satisfaction with current solution
