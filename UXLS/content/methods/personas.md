+++
date = "2017-06-15T06:22:09+01:00"
featured = false
title = "Personas"
short_title = "Personas"
weight = "100"
related_case_studies = ["persona.md"]

[header]
  thumbnail = "personas.jpg"
  image = "personas.jpg"
  summary = "Captures your user research in a format that allows you to gain a deeper understanding of your users motivations, opinions and pain points through user research."

[menucontent]
  section = "user-research"

  [menu.main]
    parent = "methods"

[sidebar]
  difficulty = "Medium"
  project_stage = "User Research"
  great_for = ["Understanding current user motivations, opinions and pain points", "Discovering new user needs for your product"]
  materials = ["Interviewer/Research Discussion Guide", "Note-taking materials", "Audio and/or video recorder (optional)"]
  resources = [""]
  takes = "3-4 weeks"

[capabilities]
  user_reqs = "yes"
  focus = "yes"
  evaluate = "no"
  generate = "no"
  measure_ux = "no"
  compare = "yes"

+++

## Overview

Personas are fictional characters which are stand-ins for user types that might use your product or service in a similar way, but they are not based solely on demographic survey data. Proper personas are created based upon direct user research and represent the users’ needs, experiences, behaviors, and goals.

### Develop personas when you want to:

- Establish a common understanding of who your “users” are
- Build empathy for your users on the project team
- Focus efforts where solutions may be applied (gap/opportunity identification)
- Drive strategic discussions by asking “how would this affect...” to define, vet, and prioritize scope.
- Verify that you have the right users selected when engaging in user experience research and/or a design activity

It should be noted, however, that while developing personas can be an effective tool to understand and drive discussions, developing and using personas should never be used as an excuse to not have additional engagements with actual users throughout the design lifecycle.

### Who's involved?

- Interviewer/Research facilitator
- Participant(s)
- Optional: 1–2 note-takers/observers (e.g. a project team member or key stakeholder)

## How To

### Plan and Prepare

1. **Identify, prioritize, and list goals to discuss and observe**\
Understand what information you need to elicit from your participants and note anything you are interested in discovering to better capture their beliefs, motivations, behaviors, interactions, and the environmental and/or business context in which they work.
2. **Identify and select participants**\
Participants should represent a variety of dimensions (e.g. length of time in role, location, and function) based on the goals.
3. **Determine logistics and schedule**\
Ideally plan to conduct the sessions where the participants naturally work to better understand how they work.
4. **Develop Interviewer/Research Discussion Guide**\
This guide will help you conduct the interview by providing an at-a-glance view of participant instructions, key questions, and additional areas of interest.

#### Tips:
- Getting management buy-in/approval can make it easier to find the right people to interview and observe.
- Select participants who are comfortable being observed and talking about themselves and their work.
- When scheduling the research, be sure to explain the goals of the research and the topics to be discussed.
- Leave breaks between research sessions to allow for any overrun and give you time to finish up your notes.
- If you plan to record the session or take photos, be sure to follow any policies, rules or practices around informed consent.

### Run the Session
1. **Meet the participant (ideally where they work)**\
It is useful to understand the contextual setting where work is done, and environmental artifacts may serve to prompt participant memories and/or areas to probe.
2. **Provide an overview of the session using your Interviewer/Research Discussion Guide**\
Let them know that any input will be kept anonymous, and that their honest input is very important.
3. **Use your Interviewer Guide to ask questions**\
Listen carefully — be prepared to go a little astray if the conversation is heading in an interesting direction. Just make sure to cover all the areas you need to.
4. **Capture key points as well as some direct quotations that can be referenced later**\
Note any important or interesting observations, behaviors, flow of work, workarounds, reference materials, etc. Particularly note any challenges they face and why.
5. **Close the session and thank the participant**\
Provide any closing remarks and give the participant your contact details, in case they think of something after the interview that they forgot to mention.

#### Tips:
- Be mindful of others in the participant’s environment when running the session, to avoid disturbance. If such a disruption might occur you may need to run a discussion session in a separate environment and ask to receive a tour with minimal discussion.
- Avoid leading questions: rather than asking “How often do you use Facebook?”, ask “Do you use any social networks?”. Not only does this give you opportunities to ask follow up questions but it can uncover additional areas to discuss.
- Avoid interrupting. Silence can be useful by giving the participant space to think and respond thoughtfully.

### Analyze and Report
1. **Analyze, summarize and categorize key observations, trends, patterns, and comments based on notes and recordings**\
Note in particular the trends in the commonalities and differences.
2. **Form user groups based on meaningful distinctions**\
This may be based on roles, attitudes, styles of work or other dimensions that allow you to make useful distinctions between groups.
3. **Flesh out the persona details based on group commonalities**\
Particularly consider tools, behaviors, attitudes, and challenges faced.
4. **Share the persona with developers, project team, or other key stakeholders**\

#### Tips:
- Including a generic but representative stock photo (NOT a real participant) helps to add attachment to the persona.
- Adding a day-in-the-life narrative helps add context to the persona’s user experience and challenges.

## Tips for Life Sciences

- Make sure you are briefed on any specific dangerous substances or microorganisms, personal protective equipment, etc. that will be in use, to avoid disruption.
- In research sessions, understanding the scientists’ key research and interests is helpful to develop a deeper understanding of their behaviors and motivations.
- It's very acceptable to not have a relevant science background as it allows you to ask questions without being too restricted by prior knowledge. Make sure to mention this explicitly at the beginning of the interview to set expectations.
- Consider partnering with a subject matter expert to help with the research if the domain is complex, requires a deeper understanding, or if it would help you establish credibility with the participant.
