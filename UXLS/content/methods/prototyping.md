+++
date = "2017-06-15T00:06:41+01:00"
featured = true
title = "Prototyping"
short_title = "Prototyping"
weight = "200"
related_case_studies = ["prototyping.md"]

[header]
  thumbnail = "prototyping.jpg"
  image = "prototyping.jpg"
  summary = "Making design concepts real can help you communicate, get feedback, and ensure you’re designing the right solutions."

[menucontent]
  section = "ui-design"

  [menu.main]
    parent = "methods"

[sidebar]
  project_stage = "UI Design"
  difficulty = "Low - Medium"
  great_for = ["Getting early user feedback", "Testing alternative design options", "Communicating with stakeholder/project team"]
  materials = ["Paper and pencil (for paper prototypes)", "Prototyping/development software (for digital prototypes)"]
  resources = [""]
  takes = "1-3 weeks"

[capabilities]
  user_reqs = "no"
  focus = "no"
  evaluate = "yes"
  generate = "yes"
  measure_ux = "no"
  compare = "no"
  
+++

## Overview

A prototype is an early mockup of a product, built to evaluate the concept or design with users. Prototypes can be paper-based or digital, with varying degrees of interactivity. They are often thrown away when done.

### Who’s involved?

- Prototype Designer
- Graphic Designer (as needed)
- Software Developer (as needed)

## How To

1. **Determine the goals you are trying to achieve with your prototype**\
Refer to the Prototyping Planning Guide to clarify what you are going to build.
2. **Define the scope of the prototype**\
Determine how much needs to be built to meet your goals and which tool you will use.
3. **Plan your design**\
Think through the overall structure, layout, navigation, interactive components, content, visual treatment, and any other elements, based on your goals.
4. **Build the prototype**\
Review and refine the prototype along the way to ensure it is complete enough to meet the goals.
5. **Use the prototype with your intended audiences to accomplish the prototyping goals**\
This may include design walkthroughs, usability tests, stakeholder demonstrations, communicating with the team, etc.
6. **Collect and analyze feedback**\
Identify key findings, recommendations, and next steps.
7. **Consider additional prototyping activities, as needed**\
This may include building a more elaborate prototype, developing “competing” prototypes to explore design options, shifting from simple prototyping tools (e.g. paper and pencil) to more sophisticated tools, etc.

#### Tips:

- Build only what you need to support your prototyping goals. You can always build more later.
- Paper prototyping is a great way to test core concepts, basic flow, and overall value without having too much detail get in the way.
- Avoid getting overly attached to the prototype, because the design will likely change over time.

## Tips for Life Sciences

- When possible, use realistic data in your prototype to build credibility of the design and better engage scientists.
- You may need to do some background research to better understand the specific subject matter and context of use.
- Review the prototype with a subject matter expert to identify any inaccurate content prior to showing the prototype your intended audiences.
- Engage the audience with the prototype in the environment where it will be used. If in a lab, consider any environmental/safety implications (e.g. personal protective equipment)


## Quote

_“If a picture is worth 1000 words, a prototype is worth 1000 meetings.”_ – saying @ideo

## Resources

- [Prototyping Planning Guide (docx)](prototyping_planning_guide.docx) - Use this 3-step guide to help identify your prototyping goals, high-level scope, and intended level of quality.
- [Free paper prototype paper templates (pdf)](https://marvelapp.com/static/site/downloads/devices.pdf) - iPhone, iPad and Apple watch templates for paper prototyping.

