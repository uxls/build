+++
date = "2017-06-07T07:53:44+02:00"
featured = true
title = "User Interviews"
short_title = "User Interviews"
weight = "100"
series = [ "user-research" ]
categories = [ "" ]
tags = [ "" ]
related_case_studies = ["user-interview.md"]

[header]
  thumbnail = "user_interviews.jpg"
  image = "user_interviews.jpg"
  summary = "There's no better way to understand the goals, challenges, and needs of your users than by talking directly with them."

[menucontent]
  section = "user-research"

[menu.main]
    parent = "methods"

[sidebar]
  project_stage = "User Research"
  difficulty = "Low"
  great_for = ["Understanding current user motivations, opinions, and pain points",
   "Discovering new user needs for your product"]
  materials = ["Interviewer Guide", "Note-taking materials", "Audio recorder (optional)"]
  takes = "1-2 weeks"
  resources = []

[capabilities]
  user_reqs = "yes"
  focus = "yes"
  evaluate = "yes"
  generate = "no"
  measure_ux = "yes"
  compare = "yes"

+++

## Overview

A user interview is a technique for gathering qualitative information, either from existing or potential users. Project team members do not represent the actual users of the product. Therefore, one of the best methods to understand users is to speak with them directly.

### Interview topics may include:

* The user’s role, domain knowledge, and experience
* Attitudes, expectations, frustrations, and unmet needs
* Environmental or contextual factors (mobility, distractions, hazards, concurrent or competing tasks, interaction with other people, tools or systems)
* Current products, devices or connectivity
* Specific user input needed to make design decisions

### Who’s involved:

* Interviewer
* Participant
* Optional: 1--2 note-takers/observers (e.g. a project team member or key stakeholder)

## How To

### I. Plan and Prepare

1. **Identify and prioritize goals of the interviews**\
  Understand what information you need to elicit from your users.
2. **Identify and select participants**\
  Participants should represent a variety of dimensions (e.g. length of time in role, location) based on the goals.
3. **Determine logistics and schedule**\
  Plan to conduct the interviews where participants naturally work.
4. **Develop Interviewer Guide**\
  This guide will help you conduct the interview by providing an at-a-glance view of participant instructions, key questions, and additional areas of interest.

#### Tips:

- Getting management buy-in/approval can make it easier to find the right people to interview.
- When scheduling the interviews, be sure to explain the goals of the interview and the topics to be discussed.
- Leave breaks between interviews to allow for any overrun, give you time to finish up your notes, and to have a bit of a break

### II. Run the Session

1. **Greet the participant and provide an overview of the session using your Interviewer Guide**\
  Let them know that any input will be kept anonymous, and that their honest input is very important.
2. **Use your Interviewer Guide to ask questions**\
  Listen carefully --- be prepared to go a little astray if the conversation is heading in an interesting direction. Just make sure to cover all the areas you need to.
3. **Capture key points as well as some direct quotations that can be referenced later**\
4. **Close the session and thank the participant**\
  Give the participant your contact details, in case they think of something after the interview that they forgot to mention.

#### Tips:

- Be aware of any company policies or relevant regulations around incentives (if using), audio/video recording, and consent for participation. Rules vary, so make sure you are compliant.
- Avoid leading questions: Rather than asking “How often do you use Facebook?”, ask “Do you use any social networks?”. Not only does this give you opportunities to ask follow up questions but it can uncover additional areas to discuss.
- Avoid interrupting. Silence can be useful by giving the participant space to think and respond thoughtfully.


### III. Analyze and Report

1. **Compile and review all of the notes and any recordings that were made**\
2. **Analyze, summarize and categorize key observations, trends, patterns, comments that align to the original goals**\
  Note anything unexpected but insightful. Identify potential key quotes that sum up a particular point.
3. **Document the set of prioritized findings and recommendations**\
  In general, findings are facts supported by data; recommendations are suggestions for improvement.
4. **Share the report with developers, project team or other key stakeholders**\
5. **Work with the project team to determine any next steps in response to the report**

#### Tips:

- Aim to do the analysis of your findings directly after each interview and sum up at the end of the day, while it’s fresh in your mind.
- Be sure to anonymize your report to maintain the privacy of your participants.

## Tips for Life Sciences

- Show a willingness to learn and an enthusiasm for their scientific work.
- In interviews you have an opportunity to understand the scientists’ research interests, and their key research questions. These can be helpful for understanding deep underlying motivations which your product could potentially address.
- If you don’t have a relevant science background, that’s okay because it allows you to ask questions without being too restricted by your prior knowledge. Just make sure to mention this explicitly at the beginning of the interview to set expectations.
- Consider partnering with a subject matter expert to help with the interview if the domain is complex, requires a deeper understanding, or if it would help you establish credibility with the interviewee.
- Consider your interviewee’s work location and context (office or laboratory). Different kinds of laboratories have different requirements around personal protective equipment (clothing, gloves, goggles, etc.). Plan accordingly.

## Resources

- [Interviewer Guide (doc)](interviewer_guide.docx) - A template with sample questions to help you plan and conduct interview sessions.
