+++
date = "2017-06-15T00:06:41+01:00"
featured = false
title = "UX Metrics Using HEART"
short_title = "HEART"
weight = "100"
related_case_studies = ["heart.md"]

[header]
  thumbnail = "heart.jpg"
  image = "heart.jpg"
  summary = "The HEART framework is an approach to define user experience measures for a system, product, or feature."

[menucontent]
  section = "evaluation"

  [menu.main]
    parent = "methods"

[sidebar]
  project_stage = "Evaluation"
  difficulty = "Medium"
  great_for = ["Determining project-specific UX metrics", "Gaining team and sponsor alignment on UX-related success"]
  materials = ["Whiteboards or flip charts", "HEART Framework Presentation", "HEART Workbook"]
  resources = [""]
  takes = "1-2 weeks"

[capabilities]
  user_reqs = "no"
  focus = "no"
  evaluate = "yes"
  generate = "no"
  measure_ux = "yes"
  compare = "no"

+++

## Overview

The HEART framework was originally developed by Google ([see here](https://research.google.com/pubs/archive/36299.pdf)) as an approach to define user experience measures for a system, product, or feature. The dimensions of the framework are:

|This HEART Dimension...  |... Answers the question            |
|-------------------------|------------------------------------|
|Happiness                |How do you know users are satisfied?|
|Engagement               |How deeply are users connected to the product or feature?|
|Adoption                 |How quickly is the new product or feature used initially?|
|Retention                |How is usage sustained over time?|
|Task Success             |How effectively do users complete  key activities?|


Typically, the HEART framework is used to determine a set of meaningful UX metrics through a series of facilitated team sessions.

### Who’s involved?

- Facilitator
- Team Lead
- Key Team Members and/or stakeholders

## How To
### Plan and Prepare
1. **Read the original Google paper: _“Measuring the User Experience on a Large Scale: User-Centered Metrics for Web Applications”_**\
This important step will ensure a solid understanding of the HEART method.
2. **Review the HEART Framework presentation and the team’s project goals with Project Lead**\
This establishes a basic understanding of the framework.
3. **Identify session participants**\
Include key stakeholders and try to limit the overall number of participants.
4. **Schedule the working sessions**\
Consider scheduling a series of sessions with a few days between each session. Participants then have adequate time to think about any considerations, implications, challenges, etc., associated with any goals, signals, or metrics.

#### Tips:
- If possible, limit the number of session participants to less than 10 (including facilitator).  This will help keep the group focused and involved.
- In general, working sessions last 90 mins to 2 hours, based on number of participants, goals, and discussion time.

### Conduct Kick-off Session
1. **Review HEART framework presentation and prioritize project goals with the session participants.**\
2. **From the set of prioritized project goals, identify the goals that rely strongly on the user experience.**\
These are the goals you will work with in the next sessions.

### Conduct Session to Identify Potential UX Metrics
1. **Map a set of selected project goals from the kickoff session to the HEART metrics**\
As an example, a project goal like “Application is trusted and easy-to-use” could map to the Happiness and Task Success metric, depending on the context.
2. **For each mapped goal, identify signals**\
_A signal is an indicator of what success or failure might look like in terms of user behaviors and/or attitudes._ For example, a signal that the “Application is trusted and easy-to-use” might be “user perception”.
3. **For each signal, identify how you might measure it over time**\
These will be your potential metrics.
4. **Wrap up the session**\
In preparation for the next session, ask participants to review the mapped set of goals, signals, and metrics to ensure completeness.

#### Tips:
- Review the example in the HEART Workbook to see details on mapping goals, signals, and metrics
- Use the HEART Workbook during the session to document the goal mapping, signals and metrics.
- Allow participants adequate time to review outcomes from this session and prepare for the next session.

### Conduct Session to Prioritize and Select Metrics
1. **Review the mapping of goals, signals, and metrics**\
Discuss and adjust any details as needed, based on input from participants.
2. **For each metric estimate the effort and value to collect the associated data as high, medium, or low**\
3. **Based on the estimates of _effort_ and _value_, prioritize and select a set of UX metrics**\
When prioritizing and selecting metrics, aim for a final set of metrics that includes all the relevant dimensions of the HEART framework.  These are your finalized, goal-aligned UX metrics.
4. **Agree on next steps to implement the data collection, reporting, etc. associated with the final set of metrics**\

#### Tips
- Use the HEART Workbook during the session to document the goal mapping, signals and metrics.
- Less is more.  It’s better to aim for fewer (more meaningful) metrics, based on the goals.
- During the discussion, note any questions, constraints, relationships, risks, or actions needed.

## Tips for Life Sciences
- If the product you are collecting metrics on will be used in the lab, ensure that at least one of the participants in this process is a lab scientist or technician to help the team identify and prioritize the most useful metrics.
- UX metrics often involve a mix of qualitative and quantitative metrics.  Avoid focusing only on quantitative (which may be a natural tendency).

## Resources
- [Heart framework (pptx)](heart_framework.pptx) - Use this presentation in your HEART kick-off session to engage your stakeholders.
- [Heart workbook (xlsx)](heart_workbook.xlsx) - Capture HEART goals, signals, and metrics for your project using this template.
