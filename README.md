# User Experience for Life Sciences

This repository contains the offical code for the UXLS toolkit pages. You can view the current status of the `dev` branch here: [beta.uxls.org](http://beta.uxls.org). The `master` branch is automatically uploaded to [uxls.org](https://uxls.org) using GitLab CI. Issues will be managed through the Pistoia Alliance JIRA instance.

For any inquiries about the code you see on GitLab, please contact [code@uxls.org](mailto:code@uxls.org). For any inquiries about the project or the website, please contact our project manager [Paula de Matos](paula.dematos@pistoiaalliance.org).