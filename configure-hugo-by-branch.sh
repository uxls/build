#!/bin/bash

BRANCH=$1

GANALYTICS_DEV="UA-101600544-1"
GANALYTICS_MASTER="UA-113162975-1"

if [ "$BRANCH" == "" ]
then
	echo No branch selected, aborting ...
	exit 1
fi

if [ "$BRANCH" == "dev" ]
then
	echo "Configuring Google Analytics for branch $BRANCH ..."
	sed -i "s/^#googleAnalytics.*/googleAnalytics = \"$GANALYTICS_DEV\"/" UXLS/config.toml
fi

if [ "$BRANCH" == "master" ]
then
	echo "Configuring Google Analytics for branch $BRANCH ..."
	sed -i "s/^#googleAnalytics.*/googleAnalytics = \"$GANALYTICS_MASTER\"/" UXLS/config.toml
fi
